import React from 'react';

const style = {
    border: '2px solid black',
    width: '10%',
}
const userInput = (props) => {
    return (
        <input type="text" onChange={props.changed} value={props.name} style={style} />
    )
}

export default userInput