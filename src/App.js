import React, { Component } from 'react';
import './App.css';
import UserInput from './UserInput/UserInput'
import UserOutput from './UserOutput/UserOutput'

class App extends Component {
  state = {
    username: ['Darya', 'Mary']
  }

  nameChangeHander = () => {
    this.setState
    ({username: ['Diane', 'Fiona']})
  }

  nameEnteredHandler = (event) => {
    this.setState 
    ({username: ['Diane', event.target.value]})
  }

  render() {
    return (
      <div className="App">
      <UserInput changed={this.nameEnteredHandler} name={this.state.username[1]} />
      <UserOutput name={this.state.username[0]} click={this.nameChangeHander} />
      <UserOutput name={this.state.username[1]} />
      </div>
    );
  }
}

export default App;
