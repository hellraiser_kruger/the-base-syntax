import React from 'react';
import './UserOutput.css'

const userOutput = (props) => {
    return(
    <div className="UserOutput">
        <p onClick={props.click}>My name is {props.name}. Below paragraph is just Lorem Ipsum :)</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fringilla eget lacus eu placerat. Nullam justo neque, volutpat sed orci fermentum, fermentum dapibus ante. Ut nulla augue, euismod ac libero eget, mattis pretium magna. Sed id mi ultrices, tincidunt nisl sed, sollicitudin arcu.</p>
    </div>
    )
}

export default userOutput